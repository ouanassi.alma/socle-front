FROM node:12.7-alpine

# Install global npm packages
RUN  npm install -g @angular/cli @ionic/cli

# Set working directory
WORKDIR /usr/src/app

# install and cache app dependencies
COPY package*.json ./
RUN npm install

# add app
COPY . .

